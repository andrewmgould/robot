const NORTH = 0;
const EAST = 90;
const SOUTH = 180;
const WEST = 270;

module.exports.DIRECTIONS = {NORTH, EAST, SOUTH, WEST};
module.exports.NUMERIC_DIRECTIONS = {
    0: 'NORTH',
    90: 'EAST',
    180: 'SOUTH',
    270: 'WEST'
};
